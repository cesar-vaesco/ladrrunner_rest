GetUsers()
{
	// Script para servico "Listar usuarios"
	/*
	 Funciones para crear una solicitud
	 
	 web_url   				--->  GEt request
	 web_cubmit_data   		--> POST / DELETE / PUT Request
	 web_custom_request 	--> SOAP Service
	 */
	
	
	/* Creaci�n de check point 
	 	
	 Pasos:
			En funci�n web_reg_find 
	 	1.- Asignar un texto como coincidencia
		2.- Salvar el conteo de la coincidencia
		3.- Crear una condici�n para validar la cantidad de usuarios esperados
		4.- Convertir LRParametro en un valor entero --> (fNameCount) su valor actual es un string
				LR Parametro --->>> String  ---->>> Integer 
				{LRParametro} --->> lr_eval_string  --->>> atoi
	 */

	web_reg_find("Text=first_name", //Esta funci�n solo afecta a la funci�n inmediata que se encuentra en el orden de compilaci�n
				"saveCount=fNameCount",	
	             LAST);
	
	//lr_start_transaction("<NombreProyecto><NombreTransacci�n><NombreServicio>");
	lr_start_transaction("reqres.in_T1_ListUsers");
	web_custom_request("LIST USERS",
	                   "Method=GET",
	                   "URL=https://reqres.in/api/users?page=2",
	                   LAST);
	
	
	
	if( atoi(lr_eval_string("{fNameCount}")) > 0)
	{
		lr_end_transaction("reqres.in_T1_ListUsers",LR_AUTO);
	}
	else 
	{
		lr_end_transaction("reqres.in_T1_ListUsers", LR_FAIL);
	}
	
	

	
	
	/*
	lr_start_transaction("reqres.in_T2_LISTUSERS_web_url");
	web_url("LIST USERS_web_url",
		"URL=https://reqres.in/api/users?page=1",
		"TargetFrame=",
		"Resource=0",
		"Referer=",
		LAST);
	lr_end_transaction("reqres.in_T2_LISTUSERS_web_url", LR_AUTO);


	lr_start_transaction("reqres.in_T3_Single_User");
	web_url("Single user",
		"URL=https://reqres.in/api/users/2",
		LAST);
	lr_end_transaction("reqres.in_T3_Single_User", LR_AUTO);

	*/
	
	return 0;
}
