Edit_User()
{
	
	/* Edita usuarios
	 Agregar el Content-Type permite que en la respuesta obtengamos los datos de nuestro recurso actualizados
	 */
	
	
	web_add_auto_header("Content-Type","application/x-www-form-urlencoded");

	//lr_start_transaction("<NombreProyecto><NombreTransacción><NombreServicio>");
	lr_start_transaction("Edit_User_T1_EditUser");
	
	web_custom_request("EditUser",
		"URL=https://reqres.in/api/users/2",
		"Method=PUT",
		"Body={"
    		"\"name\":\"Cesar\","
   			"\"job\":\"Becario Performance\""
			"}",
		LAST);
	
	lr_end_transaction("Edit_User_T1_EditUser", LR_AUTO);

	
	return 0;
}
