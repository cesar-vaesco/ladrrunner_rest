CreateUsers()
{
	
	/*
	 
	 web_custom_request()
	 
	 */
	
	//lr_start_transaction("<NombreProyecto><NombreTransacci�n><NombreServicio>");
lr_start_transaction("CreateUsers_T1_Post_User");

	web_add_auto_header("Content-Type","application/x-www-form-urlencoded");

	web_custom_request("Create User",
	                   	"Method=POST",
						"URL=https://reqres.in/api/users",	
						"Body={"
								"\"name\":\"Cesar\","
								"\"job\":\"Becario Tester\""
								"}",
						LAST);
						
lr_end_transaction("CreateUsers_T1_Post_User", LR_AUTO);

	/*
	
	En load runner se escapa por temas del compilador, permite delimitar los valores y evitar que al leer el compilador
	el c�digo lo tome como valores de una sola l�nea 
	 
	 Pasos para escapar el cuerpo de la respuesta
	 
	 
	 1.- Definir bien el cuerpo de la respuesta
	   
	{
		"name":"Cesar",
		"job":"Becario Tester"
		
	}
	
	
	2.- Asignar doble comillado para delimitar los valores a escapar
	
	
	{"
		""name":"Cesar"",
		""job":"Becario Tester""
		
	}"
	
	3.- Delimitar los valores a escapar en el cuerpo de la respuesta
	
	{"
		"\"name\":\"Cesar"\",
		"\"job\":\"Becario Tester"\"
		
	}"
	
								
	 */
	
	return 0;
}
